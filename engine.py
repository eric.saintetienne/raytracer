from image import Image
from ray import Ray
from point import Point
from color import Color, BLACK, RED, GREEN, BLUE, WHITE

from progressbar import ProgressBarPercent

class RenderEngine():
    """Renders 3Dobjects into a 2D plane using ray tracing"""

    MAX_DEPTH = 99
    MIN_DISPLACE = 0#.001
    ATTENUATION = 0.9

    def render(self, scene):
        width, height = (scene.width, scene.height)
        aspect_ratio = width / height
        x0, x1 = (-1.0, 1.0)
        y0, y1 = (-1.0/aspect_ratio, 1.0/aspect_ratio)
        xstep, ystep = (x1 - x0) / (width - 1), (y1 - y0) / (height- 1)

        camera = scene.camera
        pixels = Image(width, height)

        with ProgressBarPercent(width * height, prefix="Ray tracing...", size=100) as bar:
            for j in range(height):
                y = y0 + j * ystep
                for i in range(width):
                    x = x0 + i * xstep
                    ray = Ray(camera, Point(x, y) - camera)
                    pixels.pixels[height-j-1][i] = self.ray_trace(ray, scene)
                bar.show_next(increment=width)
        return pixels

    def ray_trace(self, ray, scene, depth=0):
        # Find the nearest object hit by the ray in the scene
        dist_hit, obj_hit = self.find_nearest_object(ray, scene)
        if obj_hit is None:
            return BLACK
        hit_pos = ray.origin + ray.direction * dist_hit
        hit_normal = obj_hit.normal(hit_pos)
        color = self.color_at(obj_hit, hit_pos, hit_normal, scene)
        if depth < self.MAX_DEPTH:
            new_ray_pos = hit_pos + hit_normal * self.MIN_DISPLACE
            new_ray_dir = ray.direction - 2 * (ray.direction * hit_normal) * hit_normal
            new_ray = Ray(new_ray_pos, new_ray_dir)
            # Reflected ray is attenuated
            color += self.ray_trace(new_ray, scene, depth+1) * obj_hit.material.reflection
        return color

    def find_nearest_object(self, ray, scene):
        dist_min, obj_hit = float("inf"), None
        for obj in scene.objects:
            dist = obj.intersects(ray)
            if dist is not None:
                if dist < dist_min:
                    dist_min, obj_hit = dist, obj
        return dist_min, obj_hit


    def color_at(self, obj_hit, hit_pos, hit_normal, scene):
        material = obj_hit.material
        obj_color = material.color_at(hit_pos)
        to_cam = scene.camera - hit_pos
        specular_k = material.specular_k
        color = material.ambient * WHITE
        # Light calculations
        for light in scene.lights:
            to_light = Ray(hit_pos, light.position - hit_pos)
            # Diffuse shading (Lambert cosine)
            color += obj_color * material.diffuse * max(hit_normal * to_light.direction, 0)
            # Specular shading (Blinn-Phong)
            half_vector = (to_light.direction + to_cam).normalize()
            color += light.color * material.specular * max(hit_normal * half_vector, 0) ** specular_k
        return color
