from color import WHITE

class Light:
    """A point light source of a given color"""
    def __init__(self, position, color=WHITE):
        self.position = position
        self.color = color
