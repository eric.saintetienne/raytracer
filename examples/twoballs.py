from point import Point
from vector import Vector

from color import Color, BLACK, WHITE, RED, GREEN, BLUE
from image import Image

from light import Light
from material import Material, ChequeredMaterial
from objects import Sphere

size=3
WIDTH = int(192*size)
HEIGHT = int(108*size)

PlaneMaterial = ChequeredMaterial(
    color=Color.from_hex("#420500"),
    color2=Color.from_hex("#e6b87d"),
    diffuse=0.5,
    ambient=0.2,
    reflection=0.3,
    scaling=4
)

RENDERED_IMG = "2balls.ppm"
CAMERA = Vector(0, 0.35, -1)
OBJECTS = (
    # Ground plane
    Sphere(center=Point(0, -10000.5, 1), material=PlaneMaterial, radius=10000),

    # Red ball
    Sphere(center=Point(-0.75, 0.1, 0.75), material=Material(Color.from_hex("#ff4080")), radius=0.6),

    # Blue ball
    Sphere(center=Point(0.75, 0.5, 1), material=Material(Color.from_hex("#0040ff")), radius=0.6),
)
LIGHTS = (
    Light(Point(1.5, 0.5, -10)),
    Light(Point(-0.5, 10.5, 0), Color.from_hex("#E6E6E6")),
)
