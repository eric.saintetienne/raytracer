from math import sqrt

class Object:
    def __init__(self, center, material):
        self.center = center
        self.material = material

class Sphere(Object):
    def __init__(self, *, radius, **kwargs):
        super().__init__(**kwargs)
        self.radius = radius

    def intersects(self, ray):
        """
        Checks whether ray intersects with the sphere
        Returns distance or None if not intersecting.
        """
        sphere_to_ray = ray.origin - self.center
        b = 2 * (ray.direction * sphere_to_ray)
        c = sphere_to_ray * sphere_to_ray - self.radius**2
        delta = b**2 - 4 * c

        if delta >= 0:
            dist = max(0, (-b -sqrt(delta))/2)
            if dist > 0:
                return dist

    def normal(self, surface_point):
        """return surface normal (vector) to the point on sphere's surface"""
        return (surface_point - self.center).normalize()
