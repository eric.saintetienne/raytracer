from math import floor

from color import WHITE, BLACK
from point import Point

class Material:
    """Material has color and properties which govern how an object reacts to light"""

    def __init__(self, color=WHITE, ambient=0.05, diffuse=1, specular=0, k=0, reflection=0.1):
        self.color = color
        self.ambient = ambient
        self.diffuse = diffuse
        self.specular = specular
        self.specular_k = k
        self.reflection = reflection

    def color_at(self, surface_point):
        return self.color

class ChequeredMaterial(Material):
    """Material with a chessboard pattern"""

    def __init__(self, *, color2=BLACK, scaling=1, **kwargs):
        super().__init__(**kwargs)
        self.color2 = color2
        self.scaling = scaling

    def color_at(self, surface_point):
        """this really works for a plane on the (x, z) axes2"""
        x, z, s = surface_point.x, surface_point.z, self.scaling
        if floor(x * s) % 2 == floor(z * s) % 2:
            return self.color
        return self.color2

