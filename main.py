#!/usr/bin/env python

import sys
import argparse
import importlib

from scene import Scene
from engine import RenderEngine

def main(filename):
    mod = importlib.import_module(filename)
    scene = Scene(
        camera=mod.CAMERA,
        objects=mod.OBJECTS,
        lights=mod.LIGHTS,
        width=mod.WIDTH,
        height=mod.HEIGHT
    )
    engine = RenderEngine()
    image = engine.render(scene)

    image.save(mod.RENDERED_IMG)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("scene", help="Path to scene file")
    args = parser.parse_args()
    if args.scene.endswith('.py'):
        args.scene = args.scene[:-3].replace('/', '.')
    main(args.scene)
