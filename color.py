from math import isclose

def parse_html(html):
    def nibble2float(c):
        h = int(c, 16)
        return ((h << 4) | h) / 255.0

    def byte2float(s):
        return int(s, 16) / 255.0

    if html.startswith('#'):
        html = html[1:]
    if len(html) == 3:
        return (nibble2float(html[0]), nibble2float(html[1]), nibble2float(html[2]))
    if len(html) == 6:
        return (byte2float(html[0:2]), byte2float(html[2:4]), byte2float(html[4:6]))
    raise Exception(f"Malformed html color: {html!r}")

class Color:
    "A RGB Color used in 3D graphics"

    def __init__(self, R=0.0, G=0.0, B=0.0):
        self.R = R
        self.G = G
        self.B = B

    @classmethod
    def from_hex(cls, html):
        r, g, b = parse_html(html)
        return cls(R=r, G=g, B=b)

    @property
    def RGB(self):
        "return a RGB tuple"
        return (int(255*self.R), int(255*self.G), int(255*self.B))

    def __mul__(self, val):
        return Color(
            R=self.R*val,
            G=self.G*val,
            B=self.B*val
        )
    __rmul__ = __mul__

    def __truediv__(self, val):
        return Color(
            R=self.R/val,
            G=self.G/val,
            B=self.B/val
        )

    def __add__(self, col):
        return Color(
            R=self.R + col.R,
            G=self.G + col.G,
            B=self.B + col.B
        )

    def __sub__(self, col):
        return Color(
            R=self.R - col.R,
            G=self.G - col.G,
            B=self.B - col.B
        )

    def __eq__(self, col):
        return isclose(self.R, col.R) and isclose(self.G, col.G) and isclose(self.B, col.B)

    def __repr__(self):
        return f"Color({self.R},{self.G},{self.B})"

    def __iter__(self):
        yield self.R
        yield self.G
        yield self.B

BLACK = Color()
RED = Color(R=1.0)
GREEN = Color(G=1.0)
BLUE = Color(B=1.0)
WHITE = RED + GREEN + BLUE
