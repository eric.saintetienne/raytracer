from math import sqrt, isclose
import numbers

class Vector:
    "A threee element vector used in 3D graphics"

    def __init__(self, x=0.0, y=0.0, z=0.0):
        self.x = x
        self.y = y
        self.z = z

    def _dot_product(self, vec):
        return self.x*vec.x + self.y*vec.y + self.z*vec.z

    def _magnitude(self):
        return sqrt(self * self)

    def normalize(self):
        return self / abs(self)

    __abs__ = __float__ = _magnitude

    def __add__(self, vec):
        if isinstance(vec, Vector):
            return Vector(x=self.x+vec.x, y=self.y+vec.y, z=self.z+vec.z)
        raise Exception(f"Cannot add {self!r} and {vec!r}")

    def __sub__(self, vec):
        if isinstance(vec, Vector):
            return Vector(x=self.x-vec.x, y=self.y-vec.y, z=self.z-vec.z)
        raise Exception(f"Cannot substract {self!r} and {vec!r}")

    def __truediv__(self, n):
        if isinstance(n, numbers.Number):
            return Vector(x=self.x/n, y=self.y/n, z=self.z/n)
        raise Exception(f"Cannot divide {self!r} and {n!r}")

    def __eq__(self, v):
        return isclose(self.x, v.x) and isclose(self.y, v.y) and isclose(self.z, v.z)

    def __mul__(self, other):
        if isinstance(other, Vector):
            return self._dot_product(other)
        if isinstance(other, numbers.Number):
            return Vector(x=self.x*other, y=self.y*other, z=self.z*other)
        raise Exception(f"Cannot multiply {self!r} and {other!r}")
    __rmul__ = __mul__

    def __repr__(self):
        return f"Vector(x={self.x},y={self.y},z={self.z})"
