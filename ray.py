class Ray:
    """Half line with an origin and a normalized direction"""

    def __init__(self, origin, direction):
        self.origin = origin
        self.direction = direction.normalize()

    def __repr__(self):
        return f"Ray(origin={self.origin!r},direction={self.direction!r})"
