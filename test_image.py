#!/usr/bin/env python

import sys
import unittest
import pytest
from unittest.mock import patch, mock_open

from image import Image

class TestImage(unittest.TestCase):

    def setUp(self):
        self.img = Image(3, 2)
        self.img.pixels[0][0] = (1, 0, 0)
        self.img.pixels[0][1] = (0, 1, 0)
        self.img.pixels[0][2] = (0, 0, 1)
        self.img.pixels[1][0] = (1, 1, 0)
        self.img.pixels[1][1] = (1, 1, 1)
        self.img.pixels[1][2] = (0.001, 0, 0)
        self.img.save("a.ppm")
        self.ppm = "P3 3 2\n255\n255 0 0\n0 255 0\n0 0 255\n255 255 0\n255 255 255\n0 0 0\n"

    # PPM representation

    def test_10_ppm(self):
        ppm = self.img.ppm()
        self.assertEqual(ppm, self.ppm)

    # File output

    def test_20_save(self):
        open_mock = mock_open()
        with patch("image.open", open_mock, create=True):
            self.img.save("out.ppm")
        open_mock.assert_called_with("out.ppm", "w")
        open_mock.return_value.write.assert_called_once_with(self.ppm)

    # Misc

    def test_99_repr(self):
        self.assertEqual(repr(self.img), "Image(3x2)")

if __name__ == "__main__":
    pytest.main(args=["-v", sys.argv[0]])
