#!/usr/bin/env python

import sys
import unittest
import pytest

from color import Color, parse_html

import color

class TestColor(unittest.TestCase):

    def setUp(self):
        self.c = Color(R=0.3, G=0.6, B=0.9)

    # Operations with Numbers

    def test_10_multiply(self):
        self.assertEqual(self.c * 1.0, self.c)
        self.assertEqual(self.c * 0.001, Color(0.0003, 0.0006, 0.0009))
        self.assertEqual(self.c * 1000, color.WHITE)

    def test_11_divide(self):
        self.assertEqual(self.c / 1.0, self.c)
        self.assertEqual(self.c / 1000, Color(0.0003, 0.0006, 0.0009))

    # Operations with Colors

    def test_20_add(self):
        self.assertEqual(self.c + color.BLACK, self.c)
        self.assertEqual(self.c + color.WHITE, color.WHITE)

    def test_21_substract(self):
        self.assertEqual(self.c - color.BLACK, self.c)
        self.assertEqual(self.c - color.WHITE, color.BLACK)

    # RGB conversion
    def test_30_rgb(self):
        self.assertEqual(self.c.RGB, (76, 153, 229))

    def test_31_constants(self):
        self.assertEqual(color.BLACK.RGB, (0,   0,   0))
        self.assertEqual(color.RED.RGB,   (255, 0,   0))
        self.assertEqual(color.GREEN.RGB, (0,   255, 0))
        self.assertEqual(color.BLUE.RGB,  (0,   0,   255))
        self.assertEqual(color.WHITE.RGB, (255, 255, 255))

    # from HTML conversion
    def test_40_html_colors(self):
        self.assertEqual(parse_html("CCC"), (0.8, 0.8, 0.8))
        self.assertEqual(parse_html("#000"), tuple(color.BLACK))
        self.assertEqual(parse_html("FFF"), tuple(color.WHITE))
        self.assertEqual(parse_html("#FF0000"), tuple(color.RED))
        self.assertEqual(parse_html("ae33de"), (0.6823529411764706, 0.2, 0.8705882352941177))

    def test_41_html_constructor(self):
        c = Color.from_hex("CCC")
        self.assertEqual(c, Color(R=0.8, G=0.8, B=0.8))
        c = Color.from_hex("#000000")
        self.assertEqual(c, color.BLACK)

    # Misc

    def test_99_repr(self):
        self.assertEqual(repr(self.c), "Color(0.3,0.6,0.9)")

if __name__ == "__main__":
    pytest.main(args=["-v", sys.argv[0]])
