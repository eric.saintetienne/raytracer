#!/usr/bin/env python

import sys
import unittest
import pytest

from vector import Vector

class TestVector(unittest.TestCase):

    def setUp(self):
        self.v1 = Vector(1.0, -2.0, -2.0)
        self.v2 = Vector(3.0, 6.0, 9.0)

    # Operations with numbers

    def test_10_multiply_number(self):
        self.assertEqual(self.v1*3, Vector(x=3, y=-6, z=-6))

    def test_11_divide_number(self):
        self.assertEqual(self.v1.normalize(), Vector(x=1.0/3, y=-2.0/3, z=-2.0/3))

    # Operations with Vectors

    def test_20_addition(self):
        self.assertEqual(self.v1 + self.v2, Vector(x=4, y=4, z=7))

    def test_21_substraction(self):
        self.assertEqual(self.v1 - self.v2, Vector(x=-2, y=-8, z=-11))
        self.assertEqual(self.v2 - self.v1, Vector(x=2, y=8, z=11))

    def test_22_dot_product(self):
        self.assertEqual(self.v1 * self.v2, -27)
        self.assertEqual(self.v2 * self.v1, -27)

    # Specific Operations

    def test_30_magnitude(self):
        self.assertEqual(float(self.v1), 3)
        self.assertEqual(abs(self.v1), 3)

    def test_31_normalize(self):
        self.assertEqual(self.v1.normalize(), Vector(x=1.0/3, y=-2.0/3, z=-2.0/3))

    # Misc

    def test_99_repr(self):
        self.assertEqual(repr(self.v1), "Vector(x=1.0,y=-2.0,z=-2.0)")

if __name__ == "__main__":
    pytest.main(args=["-v", sys.argv[0]])
