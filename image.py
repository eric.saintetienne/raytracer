from color import Color, BLACK
from progressbar import ProgressBarPercent

class Image:
    "A threee element vector used in 3D graphics"

    def __init__(self, width=320, height=200):
        self.rows = height
        self.columns = width
        self.pixels = []
        for _ in range(self.rows):
            self.pixels.append([None] * self.columns)

    def ppm(self):
        arr = [f"P3 {self.columns} {self.rows}\n255"]
        with ProgressBarPercent(self.rows * self.columns, prefix="Writing...", size=100) as bar:
            for row in self.pixels:
                for pix in row:
                    normalized = [round(max(min(p*255, 255), 0)) for p in pix]
                    arr.append(" ".join(tuple(map(str, normalized))))
                bar.show_next(increment=self.columns)
        return "\n".join(arr)

    def save(self, filename, fmt="ppm"):
        if fmt == "ppm":
            with open(filename, "w") as f:
                f.write(self.ppm())
        else:
            raise Exception(f"Unsupported image file format: {fmt}")

    def __repr__(self):
        return f"Image({self.columns}x{self.rows})"
