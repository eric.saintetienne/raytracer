import sys
import os

class BaseProgressBar:
    def __init__(self, count, prefix="", size=None, file=sys.stdout):
        self.prefix = (prefix + " ") if prefix else ""
        self.size = size
        self.count = count
        self.file = file
        self.progress = 0
        if size is None:
            self.size = os.get_terminal_size(0)[0]
        else:
            self.size = min(self.size, os.get_terminal_size(0)[0])
        self.x = None

    def show(self):
        """Calculate the new position and return True if it has changed"""
        x = (self.size * self.progress) // self.count
        if x == self.x:
            return False
        self.x = x
        return True

    def show_next(self, increment=1):
        """Increment the counter and display the bar"""
        self.progress += increment
        self.show()

    def __enter__(self):
        self.show()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.file.write("\n")

class ProgressBarNumber(BaseProgressBar):
    """Display count as absolute value"""
    def __init__(self, count, prefix="", size=None, file=sys.stdout):
        super().__init__(count, prefix=prefix, size=size, file=file)
        # Adjust size to accomodate the counter:
        self.count_len = len(str(count))
        self.size -= 2 * self.count_len + 4 + len(self.prefix)

    def show(self):
        """Display the bar unless the position hasn't changed"""
        if not super().show():
            return
        filled = '#' * self.x
        empty = '.' * (self.size - self.x)
        self.file.write(f"\r{self.prefix}[{filled}{empty}] {self.progress:>{self.count_len}}/{self.count}")
        self.file.flush()

class ProgressBarPercent(BaseProgressBar):
    """Display count in percentage"""
    def __init__(self, count, prefix="", size=None, file=sys.stdout):
        super().__init__(count, prefix=prefix, size=size, file=file)
        # Adjust size to accomodate the counter:
        self.size -=  7 + len(self.prefix)

    def show(self):
        """Display the bar unless the position hasn't changed"""
        if not super().show():
            return
        filled = '#' * self.x
        empty = '.' * (self.size - self.x)
        self.file.write(f"\r{self.prefix}[{filled}{empty}] {100 * self.progress // self.count:>3}%")
        self.file.flush()


if __name__ == "__main__":
    import time
    COUNT = 1000

    with ProgressBarPercent(COUNT, prefix=f"Fast count to {COUNT} using ProgressBarPercent") as bar:
        for _ in range(COUNT):
            time.sleep(0.001)
            bar.show_next()

    with ProgressBarNumber(COUNT, prefix=f"Fast count to {COUNT} using ProgressBarNumber") as bar:
        for _ in range(COUNT):
            time.sleep(0.001)
            bar.show_next()
